#include <stdio.h>
#include <stdlib.h>
#include <X11/Xlib.h>

int main(void)
{
        Display       *dpy;
        int           screen_num, screen_cnt, depth, conn_num;
        int           screen_width, screen_height;
        int           cell, plane;
        unsigned long white, black;
        int           proc_ver, proc_rver, vendor_re;
        char          *server_vendor, *display_str;

        dpy = XOpenDisplay(NULL);
        if (!dpy) {
                fprintf(stderr, "Can not open X Server.\n");
                exit(EXIT_FAILURE);
        }

        screen_num    = DefaultScreen(dpy);
        depth         = DefaultDepth(dpy, screen_num);
        screen_width  = DisplayWidth(dpy, screen_num);
        screen_height = DisplayHeight(dpy, screen_num);

        cell          = DisplayCells(dpy, screen_num);
        plane         = DisplayPlanes(dpy, screen_num);

        white         = WhitePixel(dpy, screen_num);
        black         = BlackPixel(dpy, screen_num);

        conn_num      = ConnectionNumber(dpy);
        screen_cnt    = ScreenCount(dpy);

        server_vendor = ServerVendor(dpy);
        display_str   = DisplayString(dpy);
        vendor_re     = VendorRelease(dpy);
        proc_ver      = ProtocolVersion(dpy);
        proc_rver     = ProtocolRevision(dpy);

        if (!XCloseDisplay(dpy)) {
                fprintf(stderr, "Can not Close the X Server.\n");
        }

        printf("BASIC INFORMATION:\n");
        printf("Screen Number    : %d\n"
               "Connection Number: %d\n"
               "Screen Count     : %d\n"
               "Depth            : %d\n"
               "Display Cells    : %d\n"
               "Display Planes   : %d\n"
               "Display String   : %s\n"
               "Screen Width     : %d\n"
               "Screen Height    : %d\n"
               "White Pixel      : %lu\n"
               "Black Pixel      : %lu\n"
               "Server Vendor    : %s\n"
               "Vendor Release   : %d\n"
               "Protocol Version : %d\n"
               "Protocol RVersion: %d\n",
               screen_num, conn_num, screen_cnt, depth, cell, plane, display_str,
               screen_width, screen_height, white, black, server_vendor, vendor_re,
               proc_ver, proc_rver);
        

        return EXIT_SUCCESS;
}
