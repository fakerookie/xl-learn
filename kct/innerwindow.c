#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <X11/Xlib.h>

int main(void)
{
        Display *dpy;
        Window root, p, w1, w2, w3;
        unsigned long black, white;

        dpy = XOpenDisplay(NULL);
        if (!dpy) {
                fprintf(stderr, "No display alivabled!\n");
                exit(EXIT_FAILURE);
        }

        root  = DefaultRootWindow(dpy);
        black = BlackPixel(dpy, 0);
        white = WhitePixel(dpy, 0);

        p  = XCreateSimpleWindow(dpy, root, 100, 100, 600, 400, 2, black, white);
        w1 = XCreateSimpleWindow(dpy, p, 50, 80, 200, 150, 2, black, white);
        w2 = XCreateSimpleWindow(dpy, p, 100, 160, 200, 150, 2, black, white);
        w3 = XCreateSimpleWindow(dpy, p, 150, 320, 200, 150, 2, black, white);

        XMapWindow(dpy, p);
        XMapWindow(dpy, w1);
        XMapWindow(dpy, w2);
        XMapWindow(dpy, w3);

        XFlush(dpy);
        sleep(10);

        XDestroySubwindows(dpy, p);
        XFlush(dpy);
        sleep(10);
        XDestroyWindow(dpy, p);

        XCloseDisplay(dpy);

        return EXIT_SUCCESS;
}