#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>

int main(void)
{
        Display              *dpy;
        Window               win;
        Font                 fnt;
        GC                   gc;
        XSetWindowAttributes win_attr;

        win_attr.override_redirect = True;

        dpy = XOpenDisplay(NULL);
        if (!dpy) {
                fprintf(stderr, "No display available!\n");
                exit(EXIT_FAILURE);
        }

        win = XCreateSimpleWindow(dpy, RootWindow(dpy, 0), 80, 80, 700, 800, 5,
                                  BlackPixel(dpy, 0), WhitePixel(dpy, 0));

        XChangeWindowAttributes(dpy, win, CWOverrideRedirect, &win_attr);
        XMapWindow(dpy, win);

        gc  = XCreateGC(dpy, win, 0L, (XGCValues *) NULL);
        fnt = XLoadFont(dpy, "fixed");
        XSetFont(dpy, gc, fnt);

        XDrawString(dpy, win, gc, 332, 400, "Hello X Server! ", 16);
        XFlush(dpy);

        sleep(15);

        XUnloadFont(dpy, fnt);
        XFreeGC(dpy, gc);
        XDestroyWindow(dpy, win);
        XCloseDisplay(dpy);

        exit(EXIT_SUCCESS);
}