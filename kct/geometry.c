#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <X11/Xlib.h>

unsigned long usr_color_pixel(Display *dpy, char *name)
{
        Colormap cmap;
        XColor c0, c1;

        cmap = DefaultColormap(dpy, 0);
        XAllocNamedColor(dpy, cmap, name, &c0, &c1);

        return (c0.pixel);
}


int main(void)
{
        Display       *dpy;
        Window        w0, w1, w2, root;
        int           w1_x, w1_y, w2_x, w2_y;
        unsigned int  width, height;
        unsigned long black, white;

        dpy = XOpenDisplay(NULL);
        if (!dpy) {
                fprintf(stderr, "No display available!\n");
                exit(EXIT_FAILURE);
        }

        black  = BlackPixel(dpy, 0);
        white  = WhitePixel(dpy, 0);
        root   = DefaultRootWindow(dpy);
        width  = 200;
        height = 100;
        w1_x   = 10;
        w1_y   = 10;
        w2_x   = -10;
        w2_y   = height - 10;

        w0 = XCreateSimpleWindow(dpy, root, 100, 100, width * 2, height * 2, 1, black, white);
        w1 = XCreateSimpleWindow(dpy, w0, w1_x, w1_y, width, height, 1, black, usr_color_pixel(dpy, "magenta"));
        w2 = XCreateSimpleWindow(dpy, w0, w2_x, w2_y, width, height, 3, black, usr_color_pixel(dpy, "blue"));

        XMapWindow(dpy, w0);
        XMapSubwindows(dpy, w0);
        XFlush(dpy);

        sleep(5);

        XUnmapWindow(dpy, w1);
        XFlush(dpy);
        sleep(5);

        XSetWindowBorder(dpy, w2, usr_color_pixel(dpy, "red"));
        XSetWindowBackground(dpy, w2, usr_color_pixel(dpy, "cyan"));
        XClearWindow(dpy, w2);
        XFlush(dpy);
        sleep(5);

        XSetWindowBackground(dpy, w0, usr_color_pixel(dpy, "blue"));
        XClearWindow(dpy, w0);
        XFlush(dpy);
        sleep(5);

        XResizeWindow(dpy, w1, width + 100, height + 50);
        // XMoveResizeWindow(dpy, w2, w2_x + 50, w1_y + 20, width - 100, height - 50);
        for (int i = 40; i < (DisplayWidth(dpy, 0) - 120); i += 50) {
                XMoveResizeWindow(dpy, w2, i, 40, width - 100, height - 50);
                XFlush(dpy);
                sleep(1);
        }
        // XFlush(dpy);
        sleep(5);
        
        XUnmapWindow(dpy, w0);
        XUnmapSubwindows(dpy, w0);
        XDestroySubwindows(dpy, w0);
        XDestroyWindow(dpy,w0);
        XCloseDisplay(dpy);

        return EXIT_SUCCESS;
}