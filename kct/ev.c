#include <stdio.h>
#include <stdlib.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>

int main(void)
{
        Display *dpy;
        XEvent  ev;
        Window  win;
        Font    fnt;
        GC      gc;

        

        dpy = XOpenDisplay(NULL);
        if (!dpy) {
                fprintf(stderr, "No display available!\n");
                exit(EXIT_FAILURE);
        }

        win = XCreateSimpleWindow(dpy, RootWindow(dpy, 0), 50, 50, 100, 200, 5, BlackPixel(dpy, 0), WhitePixel(dpy, 0));
        gc  = XCreateGC(dpy, win, 0L, (XGCValues *)NULL);
        fnt = XLoadFont(dpy, "fixed");

        XSetFont(dpy, gc, fnt);
        XSetLineAttributes(dpy, gc, 5, LineSolid, CapRound, JoinRound);
        XSelectInput(dpy, win, ExposureMask | ButtonPressMask | EnterWindowMask | LeaveWindowMask);

        XMapWindow(dpy, win);
        XFlush(dpy);

        while (True) {
                XNextEvent(dpy, &ev);

                if (ev.type == KeyPress) {
                        printf("The keycode is :%d and The key is %d\n", ev.xkey.keycode, ev.xbutton.button);
                }

                switch (ev.type) {
                case Expose:
                        printf("Expose.\n");
                        XSetForeground(dpy, gc, BlackPixel(dpy, 0));
                        XDrawString(dpy, win, gc, 50, 30, "OK!", 3);
                        break;
                case ButtonPressMask:
                        printf("Buttonpressmask.\n");
                        break;
                case EnterNotify:
                        printf("EnterNotify.\n");
                        XSetForeground(dpy, gc, BlackPixel(dpy, 0));
                        XDrawRectangle(dpy, win, gc, 10, 10, 100, 50);
                        break;
                case LeaveNotify:
                        printf("LeaveNotify.\n");
                        XSetForeground(dpy, gc, WhitePixel(dpy, 0));
                        XDrawRectangle(dpy, win, gc, 10, 10, 100, 50);
                        break;
                case KeyPress:
                        printf("The keycode is: %d and The key is %d\n", ev.xkey.keycode, ev.xbutton.button);
                        break;
                default:
                        printf("Default.\n");
                        break;
                }
        }

        XFreeGC(dpy, gc);
        XCloseDisplay(dpy);

        exit(EXIT_SUCCESS);
}