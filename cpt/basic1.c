#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xatom.h>

int main(void)
{
        int width, height;
        int screen, depth;      
        Display *dpy;
        Window root, win;
        XVisualInfo info;
        XSetWindowAttributes win_attr;
        
        
        width  = 800;
        height = 600;

        dpy = XOpenDisplay(NULL);

        if (!dpy) {
                printf("No display available\n");
                exit(EXIT_FAILURE);
        }

        root    = DefaultRootWindow(dpy);
        screen = DefaultScreen(dpy);
        depth  = 24;

        // info = {};

        if (!XMatchVisualInfo(dpy, screen, depth, TrueColor, &info)) {
                printf("No matching visal info.\n");
                exit(EXIT_FAILURE);
        }

        
        win_attr.background_pixel = 0;
        win_attr.colormap         = XCreateColormap(dpy, root, info.visual, AllocNone);

        unsigned long attr_bute_mask = CWBackPixel | CWColormap;

        win = XCreateWindow(dpy, root, 0, 0, width, height, 0, info.depth,
                            InputOutput, info.visual, attr_bute_mask, &win_attr);

        if (!win) {
                printf("Can't create Window.\n");
                exit(EXIT_FAILURE);
        }

        XStoreName(dpy, win, "Hello X");
        XMapWindow(dpy, win);
        XFlush(dpy);

        sleep(10);
        exit(EXIT_SUCCESS);
}