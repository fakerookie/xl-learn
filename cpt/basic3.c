#include <stdio.h>
#include <stdlib.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xatom.h>

static void set_size_hints(Display *dpy, Window win, int min_windth, int min_height,
                          int max_width, int max_height)
{
        XSizeHints hints = {};
        if ((min_windth > 0) && (min_height > 0)) {
                hints.flags |= PMinSize;
        }

        if ((max_width > 0) && (max_height > 0)) {
                hints.flags |= PMaxSize;
        }

        hints.min_width  = min_windth;
        hints.min_height = min_height;
        hints.max_width  = max_width;
        hints.max_height = max_height;

        XSetWMNormalHints(dpy, win, &hints);
}

int main(void)
{
        int width          = 1200;
        int height         = 800;
        int depth          = 24;
        Display *dpy       = XOpenDisplay(NULL);
        Window root        = DefaultRootWindow(dpy);
        XVisualInfo vinfo  = {};
        int default_screen = DefaultScreen(dpy);
        int window_state   = 1;
        Window               win;
        XEvent ev          = {};
        XSetWindowAttributes win_attr;

        if (!dpy) {
                fprintf(stderr, "No display available.!\n");
                exit(EXIT_FAILURE);
        }

        if (!XMatchVisualInfo(dpy, default_screen, depth, TrueColor, &vinfo)) {
                fprintf(stderr, "No matching visual info!\n");
                exit(EXIT_FAILURE);
        }

        win_attr.backing_pixel = 0;
        win_attr.colormap      = XCreateColormap(dpy, root, vinfo.visual, AllocNone);
        win_attr.event_mask    = StructureNotifyMask;

        unsigned long attr_mask = CWBackPixel | CWColormap | CWEventMask;

        win = XCreateWindow(dpy, root, 0, 0, width, height, 0, vinfo.depth,
                            InputOutput, vinfo.visual, attr_mask, &win_attr);
        if (!win) {
                fprintf(stderr, "CreateWindow error!\n");
                exit(EXIT_FAILURE);
        }

        XStoreName(dpy, win, "Hello X!");
        set_size_hints(dpy, win, 400, 300, 0, 0);
        XMapWindow(dpy, win);
        XFlush(dpy);

        while (window_state) {
                while (XPending(dpy) > 0) {
                        XNextEvent(dpy, &ev);
                        switch (ev.type) {
                        case DestroyNotify:
                                printf("XDestroywindowevent!\n");
                                XDestroyWindowEvent *de = (XDestroyWindowEvent *) &ev;
                                if (de->window == win) {
                                        window_state = 0;
                                }
                        default:
                                break;
                        }
                }
        }

        XCloseDisplay(dpy);
        exit(EXIT_SUCCESS);
}