#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xatom.h>

int main(void)
{
        int width, height;
        int screen, depth;      
        Display *dpy;
        Window root, win;
        XVisualInfo info;
        XSetWindowAttributes win_attr;
        
        
        width  = 800;
        height = 600;

        dpy = XOpenDisplay(NULL);

        if (!dpy) {
                printf("No display available\n");
                exit(EXIT_FAILURE);
        }

        root    = DefaultRootWindow(dpy);
        screen = DefaultScreen(dpy);
        depth  = 24;

        if (!XMatchVisualInfo(dpy, screen, depth, TrueColor, &info)) {
                printf("No matching visal info.\n");
                exit(EXIT_FAILURE);
        }

        
        win_attr.background_pixel    = 0;
        win_attr.colormap            = XCreateColormap(dpy, root, info.visual, AllocNone);
        win_attr.event_mask          = StructureNotifyMask;
        unsigned long attr_bute_mask = CWBackPixel | CWColormap | CWEventMask;

        win = XCreateWindow(dpy, root, 0, 0, width, height, 0, info.depth,
                            InputOutput, info.visual, attr_bute_mask, &win_attr);

        if (!win) {
                printf("Can't create Window.\n");
                exit(EXIT_FAILURE);
        }

        XStoreName(dpy, win, "Hello X");
        XMapWindow(dpy, win);
        XFlush(dpy);

        int window_state = 1;

        while (window_state) {
                XEvent ev = {};

                while(XPending(dpy) > 0) {
                        XNextEvent(dpy, &ev);
                        switch(ev.type) {
                        case DestroyNotify:
                                fprintf(stdout, "DestroyNotify!\n");
                                XDestroyWindowEvent *de = (XDestroyWindowEvent *) &ev;
                                if (de->window == win) {
                                        window_state = 0;
                                }
                        default:
                                break;
                        }
                }
        }

        sleep(10);
        XCloseDisplay(dpy);
        exit(EXIT_SUCCESS);
}